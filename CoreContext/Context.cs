﻿using Microsoft.EntityFrameworkCore;
using Models;

namespace CoreContext;

public partial class Context : DbContext
{
    public DbSet<TrackedEntity> TrackedActivities { get; set; }
    public DbSet<FoodItem> DefinedFoodItems { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TrackedEntity>()
            .ToContainer("Activities")
            .HasPartitionKey(t => t.Type)
            .HasKey(t => t.Id);

        modelBuilder.Entity<FoodItem>()
            .ToContainer("FoodItems")
            .HasPartitionKey(f => f.Name)
            .HasKey(f => f.Id);

        base.OnModelCreating(modelBuilder);
    }
}