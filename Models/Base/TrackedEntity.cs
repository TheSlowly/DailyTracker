﻿namespace Models;

public class TrackedEntity
{
    public int Id { get; set; }
    public string Type { get; set; }
}