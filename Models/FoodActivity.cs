﻿namespace Models;

public class FoodActivity : TrackedEntity
{
    public string Name { get; set; }
    public int Calories { get; set; }
}